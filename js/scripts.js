$(".slider-clients").slick({
    dots: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2500,
    variableWidth: true,
    speed: 1500,
    arrows: false
});
$("#hero-slide").slick({
    dots: false,
    infinite: true,
    speed: 500,
    fade: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 7000,
})

$(window).scroll(function(){
    if( $(this).scrollTop() > 0 ){
        $('header').addClass('scrolled');
    } else {
        $('header').removeClass('scrolled');
    }
});
$('a[href^="#"]').on('click', function(event) {

    var target = $(this.getAttribute('href'));

    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }

});
$('.dropdown-trigger').dropdown({
    hover: true
});
$('.sidenav').sidenav();
$('.collapsible').collapsible();